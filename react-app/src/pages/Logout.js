import { Navigate} from 'react-router-dom';

import {useContext, useEffect} from 'react'

import UserContext from '../UserContext';


export default function Logout(){
  
  //consume the UserContext object and destructue it to acces the user state and unsetUser function from context provider.
  const {unsetUser,setUser} = useContext(UserContext);

  // localStorage.clear()
  unsetUser();

  useEffect(() =>{
    setUser({id:null});
  })

  return(
    <Navigate to = "/"/>
  )
};
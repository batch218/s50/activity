//Userprovider
import { UserProvider } from './UserContext';
//for react
import {useState, useEffect} from 'react';

//for components 
import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView'
//for pages
import Login from './pages/Login';
import './App.css';
import Register from './pages/Register';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Logout from './pages/Logout';
import Error from './pages/Error'


//react bootstrap
import {Container} from 'react-bootstrap';

//react DOM
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {


  // State hook for the user state
  // global scope
  // This will be used to store the user information and will be used for validating if a user logged in on app or not.
  //const [user, setUser] = useState({email: localStorage.getItem('email')});
  const [user, setUser] = useState({
    id:null,
    isAdmin:null
  });



  //for clearing local storage.
  const unsetUser = () => {
    localStorage.clear();
  }

  // used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout.
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    //user is loggin in 
    .then(res => res.json())
    .then(data => {

        if(typeof data._id !== "undefined") {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        } 
           //user is loggin out
        else { 
            setUser({
                id: null,
                isAdmin: null
            })
        }
    })
}, []);
// useEffect ending

  return (
    <>
    <UserProvider value =  {{user, setUser, unsetUser}}>
    {/* Common pattern in react.js for a component to return mutiple elements. */}
    {/* Initializes the dynamic routing */}
    <Router>
      <AppNavbar />
        <Container>
         <Routes>
            <Route path ='/' element={<Home/>} />
            <Route path ='/courses' element={<Courses/>}/>
            <Route path ='/courses/:courseId' element={<CourseView/>}/>
            <Route path ='/register' element={<Register/>}/>
            <Route path ='/login' element={<Login/>}/>
            <Route path ='/logout' element={<Logout/>}/>
            <Route path ='/*' element={<Error/>} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>
    </>

  );
}

export default App;
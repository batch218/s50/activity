import React, { useState , useEffect} from 'react';
import {Button,Card} from 'react-bootstrap';

import {Link} from 'react-router-dom';

import PropTypes  from 'prop-types';


export default function CourseCard({course}) {
	const {name, description,price,_id} = course;
	//count = getter
	//setCount = setter
	// useState(0) - useState(initialGetterValue)
	// const [count, setCount] = useState(0);
	// // S51 ACTIVITY
	// const [seats, setSeats] = useState(5);
	// // S51 ACTIVITY END	
	// //console.log(useState(0));
	// const [isOpen, setIsopen] = useState(true);


	//function that keeps track of the enrolles for a course
	function enroll(){
	// 	setCount(count +1 );
	// 	console.log('Enrollees:' + count);
// S51 ACTIVITY
	// setSeats(seats - 1  );
	// console.log('Seats:' + seats);
	// if(seats === 1 ){
	// 	alert('no more seats')
	// 	document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// }
// S51 ACTIVITY END	
//================================================================


//================================================================
	}
	// useEffect allow us to execute function if the value of seats state changes.
	// useEffect(() =>{
	// 	if(seats === 0 ){
	// 			setIsopen(false)
	// 			alert('No more seats')
	// 			document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 		}
	// 		//will run anytime one of the values in the array of dependencues changes.
	// }, [seats])
	
	//Checks to see it the data was successfully passed.
	//console.log(props);
	//Every components receives information in a form of an object
	//console.log(typeof props);
	return (
		
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
					<Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
					{/* <Card.Text>Enrollees {count}</Card.Text>
					<Card.Text>Seats {seats}</Card.Text>
					<Button id={ 'btn-enroll-' + id } className="bg-primary" onClick={enroll}>Enroll</Button> */}
					
	    </Card.Body>
	</Card>
	)
}
// "propTypes" = are a good way of checking data type of information between components.
CourseCard.protoTypes = {
	// "shape" method is used to check if prop object confirms to a specific "shape"
	course: PropTypes.shape({
		//Define properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
